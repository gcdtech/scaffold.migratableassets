# Changelog

### 1.0.1

* Change:	Support for PHP 5.6
* Fixed:	Category now passed to base provider properly

### 1.0.0

* Added:	First version
