<?php

/**
 * Copyright (c) 2017 GCD Technologies Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Gcdtech\MigratableAssets\Tests;

use Codeception\Specify;
use Gcdtech\MigratableAssets\MigratableAssetCatalogueProvider;
use Gcdtech\MigratableAssets\Models\Asset;
use Rhubarb\Crown\Assets\AssetCatalogueProvider;
use Rhubarb\Crown\Assets\AssetCatalogueSettings;
use Rhubarb\Crown\Assets\LocalStorageAssetCatalogueProvider;
use Rhubarb\Crown\Assets\LocalStorageAssetCatalogueProviderSettings;
use Rhubarb\Crown\Tests\unit\Assets\AssetCatalogueProviderTests;

class MigratableAssetCatalogueProviderTest extends AssetCatalogueProviderTests
{
    use Specify;

    public function testStoringAsset()
    {
        $path = __DIR__.'/test.txt';
        file_put_contents($path, "abc123");
        
        $this->getProvider();

        $asset = AssetCatalogueProvider::storeAsset($path, AssetCatalogueProviderTests::TEST_CATEGORY);

        $this->specify("Assets create models", function() use ($asset) {
            verify(Asset::all())->count(1);
            $model = Asset::findLast();
            verify($model->AssetID)->equals($asset->getProviderData()["AssetID"]);
        });

        $this->specify("Assets have base assets", function() use ($asset) {
            $model = Asset::findLast();
            verify($model->Token)->notEmpty();
            verify($model->Category)->equals(AssetCatalogueProviderTests::TEST_CATEGORY);
            verify($model->Provider)->equals(LocalStorageAssetCatalogueProvider::class);
            verify($model->Size)->equals(6);
            verify($model->MimeType)->contains("text/plain");
            $baseAsset = AssetCatalogueProvider::getAsset($model->Token);
            verify($baseAsset->getProviderData())->hasKey("file");
        });

        $this->specify("Deleting an asset removes the asset entry", function() use ($asset){
            $asset->delete();
            verify(Asset::all())->count(0);
        });
    }

    protected function getProvider()
    {
        $settings = LocalStorageAssetCatalogueProviderSettings::singleton();
        $settings->storageRootPath = __DIR__."/data/";

        $settings = AssetCatalogueSettings::singleton();
        $settings->jwtKey = "rhubarbphp";

        AssetCatalogueProvider::setProviderClassName(MigratableAssetCatalogueProvider::class);
        MigratableAssetCatalogueProvider::setProviderClassName(LocalStorageAssetCatalogueProvider::class, AssetCatalogueProviderTests::TEST_CATEGORY);

        return new MigratableAssetCatalogueProvider();
    }
}