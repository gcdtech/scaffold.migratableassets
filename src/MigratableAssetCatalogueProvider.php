<?php

/**
 * Copyright (c) 2017 GCD Technologies Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Gcdtech\MigratableAssets;

use Rhubarb\Crown\Assets\Asset;
use Rhubarb\Crown\Assets\AssetCatalogueProvider;
use Rhubarb\Crown\Exceptions\AssetException;
use Rhubarb\Crown\Exceptions\AssetNotFoundException;
use Rhubarb\Stem\Exceptions\RecordNotFoundException;

class MigratableAssetCatalogueProvider extends AssetCatalogueProvider
{
    /**
     * A map of base providers to category names.
     *
     * Intentionally hides the base mapping array.
     *
     * @var array
     */
    private static $providerMap = [];

    /**
     * Sets the base asset provider for a given category.
     *
     * @param string $providerClassName The class to register as the base provider
     * @param string $assetCategory The category of provider - or empty for the default provider
     */
    public static function setProviderClassName($providerClassName, $assetCategory = "")
    {
        self::$providerMap[$assetCategory] = $providerClassName;
    }

    /**
     * Returns an instance of the correct base provider for a given category
     *
     * @param string $assetCategory The category of base provider - or empty for the default provider
     * @return AssetCatalogueProvider
     * @throws AssetException
     */
    public static function getProvider($assetCategory = "")
    {
        if (isset(self::$providerMap[$assetCategory])) {
            $class = self::$providerMap[$assetCategory];
        } elseif (isset(self::$providerMap[""])){
            $class = self::$providerMap[""];
        } else {
            throw new AssetException("", "No provider mapping could be found for category '".$assetCategory."'");
        }

        $provider = new $class($assetCategory);

        return $provider;
    }

    private function getBaseAsset(Asset $asset)
    {
        $model = $this->getAssetModel($asset);
        
        $baseToken = $model->Token;
        $baseAsset = AssetCatalogueProvider::getAsset($baseToken);

        return $baseAsset;
    }

    /**
     * Deletes the given asset
     *
     * @param Asset $asset
     * @return mixed
     */
    public function deleteAsset(Asset $asset)
    {
        $baseAsset = $this->getBaseAsset($asset);

        $model = $this->getAssetModel($asset);
        $model->delete();

        return $baseAsset->delete();
    }

    /**
     * Creates an asset by loading from the given file path.
     *
     * @param $filePath
     * @param $commonProperties
     * @return Asset
     */
    public function createAssetFromFile($filePath, $commonProperties)
    {
        $category = $commonProperties["category"];

        $baseProvider = self::getProvider($category);
        $baseAsset = $baseProvider->createAssetFromFile($filePath, $commonProperties);

        $model = new Models\Asset();
        $model->Token = $baseAsset->getToken();
        $model->Category = $baseAsset->category;
        $model->Provider = get_class($baseAsset->getProvider());
        $model->Size = $baseAsset->size;
        $model->MimeType = $baseAsset->mimeType;
        $model->save();

        $commonProperties["AssetID"] = $model->UniqueIdentifier;

        $token = $this->createToken($commonProperties);

        $asset = new Asset($token, $this, $commonProperties);

        return $asset;
    }

    /**
     * Gets a PHP resource stream to allow reading the asset in chunks
     * @param Asset $asset
     * @return mixed
     */
    public function getStream(Asset $asset)
    {
        $baseAsset = $this->getBaseAsset($asset);
        return $baseAsset->getStream();
    }

    /**
     * If exposable, returns a URL for giving to the client for fetching the asset
     *
     * @param Asset $asset
     * @return string
     */
    public function getUrl(Asset $asset)
    {
        $baseAsset = $this->getBaseAsset($asset);
        return $baseAsset->getUrl();
    }

    /**
     * Gets the model record for a given asset.
     *
     * @param Asset $asset
     * @return Models\Asset
     * @throws AssetNotFoundException Thrown if the asset no longer exists.
     */
    private function getAssetModel(Asset $asset)
    {
        $assetId = $asset->getProviderData()["AssetID"];

        try {
            $model = new Models\Asset($assetId);
            return $model;
        } catch (RecordNotFoundException $er){
            throw new AssetNotFoundException($asset->getToken());
        }
    }
}